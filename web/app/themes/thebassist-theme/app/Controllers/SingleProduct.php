<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleProduct extends Controller
{
    use Partials\Product;
}