<?php

namespace App\Controllers\Partials;

trait Post {
    public function recent_posts()
    {
        $args = [
            'post_type'           => 'post',
            'numberposts'         => 6,
            'orderby'             => 'date',
            'order'               => 'DESC',
        ];

        $query = new \WP_Query($args);

        $posts = [];

        foreach ($query->posts as $post) {
            $obj = (object) [
                'title' => $post->post_title,
                'content' => substr($post->post_content, 0, 200),
                'link' => $post->guid,
            ];
            array_push($posts, $obj);
        }

        return $posts;
    }
}