<?php

namespace App\Controllers\Partials;

trait Product {
    public function onsale_products()
    {
        $args = [
            'post_type'           => 'product',
            'numberposts'         => 6,
            'orderby'             => 'date',
            'order'               => 'DESC',
        ];

        $wc_products = wc_get_products($args);

        $products = [];

        for ($i = 0; $i < count($wc_products) && count($products) < 6; $i++) {
            $product = $wc_products[$i];
            if ($product->is_on_sale()) {
                $obj = (object) [
                    'id' => $product->get_id(),
                    'name' => $product->get_title(),
                    'price' => '€' . $product->get_price(),
                    'thumbnail' => get_the_post_thumbnail($product->get_id())
                ];
                array_push($products, $obj);
            }
        }

        return $products;
    }
}