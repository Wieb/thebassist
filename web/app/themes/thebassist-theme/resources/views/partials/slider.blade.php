<h1 class="sale-title">Products on sale</h1>
<div class="swiper-container">
    <div class="swiper-wrapper slider">
        @foreach ($onsale_products as $item)
        <div class="swiper-slide slider__item">
            <div class="slider__item__content">
                <h2 class="slider__item__name">{{ $item->name }}</h2>
                <h3 class="slider__item__price">{{ $item->price }}</h3>
            </div>
            <div class="slider__item__image">
                {!! $item->thumbnail !!}
            </div>
        </div>
        @endforeach
    </div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-pagination"></div>
</div>