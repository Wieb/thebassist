<article @php post_class() @endphp>
  <div class="post__content">
    <header>
      <h1 class="entry-title">{!! get_the_title() !!}</h1>
      @include('partials/entry-meta')
    </header>
    <div class="entry-content">
      @php the_content() @endphp
    </div>
    <footer>
      {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
    </footer>
    <div class="post__comments">
      @php comments_template('/partials/comments.blade.php') @endphp
    </div>
  </div>
</article>
