@extends('layouts.app')

@section('content')

    <div class="front-posts">
        @foreach ($recent_posts as $post)
        <article class="front-posts">
            <a href="{{ $post->link }}">
                <h2 class="front-posts__title">{{ $post->title }}</h2>
                <p class="front-posts__content">@php echo $post->content @endphp</p>
            </a>
        </article>
        @endforeach
    </div>

@endsection